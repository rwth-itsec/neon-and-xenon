import json
import os
import shutil
import subprocess
from base64 import b64encode, b64decode
from collections import UserList
from typing import Dict, Optional, List, Iterable, Union, Callable, Any, Hashable, Tuple
import sympy

from ProgramFiles.helper import average_and_standard_deviation, min_median_max, get_cdir, join_path_abs, NeonException
from ProgramFiles.network import Network
from ProgramFiles.operationmode import OperationMode


class ComputationReport:
    """
    The computation report is a read only report on one single MPC computation.
    It contains all NEON-Parameters as well the outputs of the computation, most of which is
    stored in the client computation reports.

    It also provides utility functions for evaluating the reported timer times. All timer functions support named
    timers, i.e. you can also provide the name of a timer to the timer parameter.
    """

    # Parameters
    __program: str
    __pre_substitution_hash: str
    __substitutions: Dict[str, str]
    __program_hash: str
    __timer_names: Dict[int, str]
    __protocol: str
    __operation_mode: OperationMode
    __delay: Optional[str] = None
    __outgoing_bandwidth: Optional[str] = None
    __incoming_bandwidth: Optional[str] = None
    __bits_from_squares: bool
    __batch_size: int
    __bucket_size: int
    __mpspdz_version: str

    __client_reports: List["ClientComputationReport"]

    __secrets_before_computation: Optional[List[int]] = None
    __secrets_after_computation: Optional[List[int]] = None

    __custom_metadata : Optional[Any] = None

    def __init__(self,
                 program: str,
                 pre_substitution_hash: str,
                 substitutions: Dict[str, str],
                 program_hash: str,
                 timer_names: Dict[int, str],
                 protocol: str,
                 operation_mode: OperationMode,
                 delay: Optional[str],
                 outgoing_bandwidth: Optional[str],
                 incoming_bandwidth: Optional[str],
                 bits_from_squares: bool,
                 batch_size: int,
                 bucket_size: int,
                 mpspdz_version: str,
                 client_reports: List["ClientComputationReport"],
                 secret_before_computation: Optional[List[int]],
                 secrets_after_computation: Optional[List[int]],
                 custom_metadata: Optional[Any]):
        self.__program = program
        self.__pre_substitution_hash = pre_substitution_hash
        self.__substitutions = substitutions
        self.__program_hash = program_hash
        self.__timer_names = timer_names
        self.__protocol = protocol
        self.__operation_mode = operation_mode
        self.__delay = delay
        self.__outgoing_bandwidth = outgoing_bandwidth
        self.__incoming_bandwidth = incoming_bandwidth
        self.__bits_from_squares = bits_from_squares
        self.__batch_size = batch_size
        self.__bucket_size = bucket_size
        self.__mpspdz_version = mpspdz_version
        self.__client_reports = client_reports
        self.__secrets_before_computation = secret_before_computation
        self.__secrets_after_computation = secrets_after_computation
        self.__custom_metadata = custom_metadata

# region Timers
    def get_timer_keys(self) -> List[int]:
        return list(self.__client_reports[0].timers.keys())

    def timer_key_from_name(self, name: str) -> Optional[int]:
        for (key, timer_name) in self.__timer_names.items():
            if name == timer_name:
                return key
        return None

    def get_timer_id(self, timer: Union[int, str]) -> int:
        """Returns the integer id of the given timer. Raises an exception if the timer name is not known."""
        if isinstance(timer, str):
            result = self.timer_key_from_name(timer)
            if result is None:
                raise NeonException("Unknown timer name.")
            return result
        else:
            return timer

    def get_timer_times(self, timer: Union[int, str]) -> List[float]:
        """Returns the reported timer times from each client. If the timer is a string, the corresponding timer wil be
        looked up. Raises an exception if the timer does not exist."""
        timer = self.get_timer_id(timer)
        return [c_report.timers[timer] for c_report in self.__client_reports]

    def get_timer_average_and_standard_deviation(self, timer: Union[int, str]) -> (float, float):
        """
        Returns a tuple with the average and the standard deviation of a (named) timer.
        """
        return average_and_standard_deviation(self.get_timer_times(timer))

    def get_timer_min_median_max(self, timer: Union[int, str]) -> (float, float, float):
        """
        Returns a tuple with the minimal, median and maximal reported times of a (named) timer.
        """
        return min_median_max(self.get_timer_times(timer))

    def get_timer_average(self, timer: Union[int, str]) -> float:
        """Returns the average of a (named) timer."""
        return self.get_timer_average_and_standard_deviation(timer)[0]

    def get_timer_standard_deviation(self, timer: Union[int, str]) -> float:
        """Returns the standard deviation of the reported times on a (named) timer."""
        return self.get_timer_average_and_standard_deviation(timer)[1]

    def get_timer_min(self, timer: Union[int, str]) -> float:
        """Returns the minimal reported time of a (named) timer."""
        return min(self.get_timer_times(timer))

    def get_timer_median(self, timer: Union[int, str]) -> float:
        """Returns the median reported time of a (named) timer."""
        return self.get_timer_min_median_max(timer)[1]

    def get_timer_max(self, timer: Union[int, str]) -> float:
        """Returns the maximal reported time of a (named) timer."""
        return max(self.get_timer_times(timer))

    def get_timer_data_sent(self, timer: Union[int, str]) -> List[str]:
        """Returns the data sent for the given (named) timer for each client."""
        timer = self.get_timer_id(timer)
        return [report.timers_data_sent[timer] for report in self.__client_reports]

    def get_timer_global_data_sent(self, timer: Union[int, str]) -> int:
        """Returns the global data sent for the given (named) timer in bytes."""
        tmp_timer_global_data = 0
        for tds in self.get_timer_data_sent(timer):
            if tds.endswith(" MB"):
                # We need to use sympy.Rational to avoid floating point errors in some (rare) cases
                tmp_timer_global_data += sympy.Rational(tds.rsplit(" ")[0]) * 10**6
            else:
                logger.critical("Data not given in MB, aborting")
                raise NeonException("Data not given in MB")
        if tmp_timer_global_data.is_integer:
            tmp_timer_global_data = int(tmp_timer_global_data)
        else:
            logger.critical(f"Timer global data (in bytes) is not an integer")
            raise NeonException("Timer global data (in bytes) is not an integer")
        return tmp_timer_global_data

    def get_total_runtimes(self) -> List[float]:
        return [client_report.total_runtime for client_report in self.__client_reports]

    def get_average_total_runtime(self) -> float:
        return average_and_standard_deviation(self.get_total_runtimes())[0]

    def get_cpu_times(self) -> List[float]:
        return [client_report.cpu_time for client_report in self.__client_reports]

    def get_average_cpu_time(self) -> float:
        return average_and_standard_deviation(self.get_cpu_times())[0]

    def get_global_data_sent(self) -> int:
        """Returns global data sent in bytes"""
        # First check if they are all the same
        tmp_all_global_data = [client_report.global_data_sent for client_report in self.__client_reports]
        if not all([glob_data == tmp_all_global_data[0] for glob_data in tmp_all_global_data[1:]]):
            raise Exception(f"Global data not all the same: {tmp_all_global_data}")

        # Get bytes
        if tmp_all_global_data[0].endswith(" MB"):
            # We need to use sympy.Rational to avoid floating point errors in some (rare) cases
            tmp_global_data = sympy.Rational(tmp_all_global_data[0].rsplit(" ")[0]) * 10**6
        else:
            logger.critical("Data not given in MB, aborting")
            raise NeonException("Data not given in MB")
        if tmp_global_data.is_integer:
            tmp_global_data = int(tmp_global_data)
        else:
            logger.critical(f"Global data (in bytes) is not an integer")
            raise NeonException("Global data (in bytes) is not an integer")
        return tmp_global_data

    def run_was_successfull(self) -> bool:
        # If there is any client computation report without a total runtime, then the execution failed
        if None in self.get_total_runtimes():
            return False
        else:
            return True
# endregion

    def to_serializable_dict(self) -> Dict[str, any]:
        return {
            'parameters': {
                'program': self.__program,
                'pre_substitution_hash': self.__pre_substitution_hash,
                'substitutions': self.__substitutions,
                'timer_names': self.__timer_names,
                'program_hash': self.__program_hash,
                'protocol': self.__protocol,
                'operation_mode': self.__operation_mode.name,
                'delay': self.__delay,
                'outgoing_bandwidth': self.__outgoing_bandwidth,
                'incoming_bandwidth': self.__incoming_bandwidth,
                'bits_from_squares': self.__bits_from_squares,
                'batch_size': self.__batch_size,
                'bucket_size': self.__batch_size,
                'mpspdz_version': self.__mpspdz_version
            },
            'client_reports': [report.to_serializable_dict() for report in self.__client_reports],
            'secrets_before_computation': self.__secrets_before_computation,
            'secrets_after_computation': self.__secrets_after_computation,
            'custom_metadata': self.__custom_metadata
        }

    def save_to_file(self, filename: str) -> None:
        with open(filename, 'w') as f:
            json.dump(self.to_serializable_dict(), f, indent=4)

    @staticmethod
    def from_serializable_dict(dict: Dict[str, any]) -> 'ComputationReport':
        parameters = dict['parameters']
        program = parameters['program']

        pre_substitution_hash = None
        if 'pre_substitution_hash' in parameters.keys():
            pre_substitution_hash = parameters['pre_substitution_hash']

        substitutions = parameters['substitutions']
        program_hash = parameters['program_hash']

        timer_names = None
        if 'timer_names' in parameters.keys():
            timer_names = {int(timer): name for (timer, name) in parameters['timer_names'].items() }

        protocol = parameters['protocol']
        operation_mode = OperationMode.from_name(parameters['operation_mode'])
        delay = parameters['delay']
        outgoing_bandwidth = parameters['outgoing_bandwidth']
        incoming_bandwidth = parameters['incoming_bandwidth']
        bits_from_squares = parameters['bits_from_squares']
        batch_size = parameters['batch_size']
        bucket_size = parameters['bucket_size']
        mpspdz_version = parameters['mpspdz_version']
        client_reports = [ClientComputationReport.from_serializable_dict(report) for report in dict['client_reports']]
        secrets_before_computation = dict.setdefault('secrets_before_computation', None)
        secrets_after_computation = dict.setdefault('secrets_after_computation', None)
        custom_metadata = dict.setdefault('custom_metadata', None)
        return ComputationReport(program,
                                 pre_substitution_hash,
                                 substitutions,
                                 program_hash,
                                 timer_names,
                                 protocol,
                                 operation_mode,
                                 delay,
                                 outgoing_bandwidth,
                                 incoming_bandwidth,
                                 bits_from_squares,
                                 batch_size,
                                 bucket_size,
                                 mpspdz_version,
                                 client_reports,
                                 secrets_before_computation,
                                 secrets_after_computation,
                                 custom_metadata)

    @staticmethod
    def from_file(filename: str) -> 'ComputationReport':
        with open(filename, 'r') as f:
            return ComputationReport.from_serializable_dict(json.load(f))

# region Properties
    @property
    def program(self) -> str:
        return self.__program

    @property
    def pre_substitution_hash(self) -> str:
        return self.__pre_substitution_hash

    @property
    def substitutions(self) -> Dict[str, str]:
        return self.__substitutions

    @property
    def program_hash(self) -> str:
        return self.__program_hash

    @property
    def timer_names(self) -> Dict[int, str]:
        return self.__timer_names

    @property
    def protocol(self) -> str:
        return self.__protocol

    @property
    def operation_mode(self) -> OperationMode:
        return self.__operation_mode

    @property
    def delay(self) -> Optional[str]:
        return self.__delay

    @property
    def outgoing_bandwidth(self) -> Optional[str]:
        return self.__outgoing_bandwidth
    
    @property
    def incoming_bandwidth(self) -> Optional[str]:
        return self.__incoming_bandwidth

    @property
    def network(self) -> Network:
        return Network(self.delay, self.incoming_bandwidth, self.outgoing_bandwidth)

    @property
    def bits_from_squares(self) -> bool:
        return self.__bits_from_squares

    @property
    def batch_size(self) -> int:
        return self.__batch_size

    @property
    def bucket_size(self) -> int:
        return self.__bucket_size

    @property
    def mpspdz_version(self) -> str:
        return self.__mpspdz_version

    @property
    def client_reports(self) -> List["ClientComputationReport"]:
        return self.__client_reports

    @property
    def secrets_before_computation(self) -> Optional[List[int]]:
        return self.__secrets_before_computation

    @property
    def secrets_after_computation(self) -> Optional[List[int]]:
        return self.__secrets_after_computation

    @property
    def number_of_parties(self) -> int:
        return len(self.__client_reports)

    @property
    def custom_metadata(self) -> Any:
        return self.__custom_metadata
# endregion


class ClientComputationReport:
    """The ClientComputation is a read only report on the client's view on one single MPC computation.
    It contains client-specific parameters and computation results, like timers or private outputs."""

    __ip: Optional[str]
    __input: Optional[str]

    __stdout: Optional[bytes]
    __stderr: Optional[bytes]

    __cpu_time: float
    __total_runtime: float
    __data_sent: Optional[str] = None
    __communication_rounds: Optional[int] = None
    __global_data_sent: Optional[str] = None
    __timers: Dict[int, float]
    __timers_data_sent: Optional[Dict[int, str]] = None
    __private_outputs: Optional[List[int]]

    def __init__(self,
                 ip: Optional[str],
                 input: Optional[str],
                 stdout: Optional[bytes],
                 stderr: Optional[bytes],
                 cpu_time: float,
                 total_runtime: float,
                 data_sent: Optional[str],
                 communication_rounds: Optional[int],
                 global_data_sent: Optional[str],
                 timers: Dict[int, float],
                 timers_data_sent: Dict[int, str],
                 private_output: Optional[List[int]]):
        self.__ip = ip
        self.__input = input
        self.__stdout = stdout
        self.__stderr = stderr
        self.__cpu_time = cpu_time
        self.__total_runtime = total_runtime
        self.__data_sent = data_sent
        self.__communication_rounds = communication_rounds
        self.__global_data_sent = global_data_sent
        self.__timers = timers
        self.__timers_data_sent = timers_data_sent
        self.__private_outputs = private_output

    def to_serializable_dict(self) -> Dict[str, any]:
        return {
            'ip': self.__ip,
            'input': self.__input,
            'stdout': b64encode(self.__stdout).decode('utf-8') if self.__stdout else None,
            'stderr': b64encode(self.__stderr).decode('utf-8') if self.__stderr else None,
            'cpu_time': self.__cpu_time,
            'total_runtime': self.__total_runtime,
            'data_sent': self.__data_sent,
            'communication_rounds': self.__communication_rounds,
            'global_data_sent': self.__global_data_sent,
            'timers': self.__timers,
            'timers_data_sent': self.__timers_data_sent,
            'private_outputs': self.__private_outputs
        }

    @staticmethod
    def from_serializable_dict(dict: Dict[str, any]) -> 'ClientComputationReport':
        ip = dict['ip'] if 'ip' in dict else None
        input = dict['input'] if 'input' in dict else None
        stdout = b64decode(dict['stdout'].encode('utf-8')) if ('stdout' in dict and dict['stdout']) else None
        stderr = b64decode(dict['stderr'].encode('utf-8')) if ('stderr' in dict and dict['stderr']) else None
        cpu_time = dict['cpu_time']
        total_runtime = dict['total_runtime']
        data_sent = dict['data_sent'] if 'data_sent' in dict.keys() else None
        communication_rounds = dict['communication_rounds'] if 'communication_rounds' in dict.keys() else None
        global_data_sent = dict['global_data_sent'] if 'global_data_sent' in dict.keys() else None
        timers = { int(key): value for (key, value) in dict['timers'].items() }
        timers_data_sent = None
        if 'timers_data_sent' in dict and dict['timers_data_sent'] is not None:
            timers_data_sent = { int(key): value for (key, value) in dict['timers_data_sent'].items() }
        private_outputs = dict['private_outputs'] if 'private_outputs' in dict else None

        return ClientComputationReport(ip,
                                       input,
                                       stdout,
                                       stderr,
                                       cpu_time,
                                       total_runtime,
                                       data_sent,
                                       communication_rounds,
                                       global_data_sent,
                                       timers,
                                       timers_data_sent,
                                       private_outputs)

    @property
    def ip(self) -> Optional[str]:
        return self.__ip

    @property
    def input(self) -> Optional[str]:
        return self.__input

    @property
    def stdout(self) -> Optional[bytes]:
        return self.__stdout

    @property
    def stderr(self) -> Optional[bytes]:
        return self.__stderr

    @property
    def cpu_time(self) -> float:
        return self.__cpu_time

    @property
    def total_runtime(self) -> float:
        return self.__total_runtime

    @property
    def data_sent(self) -> str:
        return self.__data_sent

    @property
    def communication_rounds(self) -> Optional[int]:
        return self.__communication_rounds

    @property
    def global_data_sent(self) -> str:
        return self.__global_data_sent

    @property
    def timers(self) -> Dict[int, float]:
        return self.__timers

    @property
    def timers_data_sent(self) -> Optional[Dict[int, str]]:
        return self.__timers_data_sent

    @property
    def private_outputs(self) -> Optional[List[int]]:
        return self.__private_outputs


class ComputationReportList(UserList):
    """
    A collection of multiple computation reports. Facilitates combining the information of these reports.
    """

    def __init__(self, reports: Iterable[ComputationReport] = None):
        if not reports:
            reports = []
        super().__init__(reports)

    def get_all_timer_times(self, timer: Union[int, str]) -> List[float]:
        """
        Collects the raw timer times from all computation reports. If a string is given, the function will look
        up the timer belonging to that timer name.
        """
        result = []
        for report in self:
            result.extend(report.get_timer_times(timer))
        return result

    def get_timer_average_and_standard_deviation(self, timer: Union[int, str]) -> (float, float):
        """Returns the average and standard deviation of all reported times on a (named) timer."""
        return average_and_standard_deviation(self.get_all_timer_times(timer))

    def get_timer_min_median_max(self, timer: Union[int, str]) -> (float, float, float):
        """Returns the minimal, median and maximal reported time on a (named) timer."""
        return min_median_max(self.get_all_timer_times(timer))

    def get_timer_average(self, timer: Union[int, str]) -> float:
        """Returns the average of a (named) timer."""
        return self.get_timer_average_and_standard_deviation(timer)[0]

    def get_timer_standard_deviation(self, timer: Union[int, str]) -> float:
        """Returns the standard deviation of the reported times on a (named) timer."""
        return self.get_timer_average_and_standard_deviation(timer)[1]

    def get_timer_min(self, timer: Union[int, str]) -> float:
        """Returns the minimal reported time of a (named) timer."""
        return min(self.get_all_timer_times(timer))

    def get_timer_median(self, timer: Union[int, str]) -> float:
        """Returns the median reported time of a (named) timer."""
        return self.get_timer_min_median_max(timer)[1]

    def get_timer_max(self, timer: Union[int, str]) -> float:
        """Returns the maximal reported time of a (named) timer."""
        return max(self.get_all_timer_times(timer))

    def get_total_runtimes(self) -> List[float]:
        result = []
        for report in self:
            result.extend(report.get_total_runtimes())
        return result

    def get_total_runtime_average_and_standard_deviation(self) -> (float, float):
        return average_and_standard_deviation(self.get_total_runtimes())

    def get_total_runtime_min_median_max(self) -> (float, float, float):
        return min_median_max(self.get_total_runtimes())

    def filter(self, f: Callable[[ComputationReport], bool]) -> "ComputationReportList":
        """Returns a ComputationReportList containing a reports that satisfy the filter."""
        result = ComputationReportList()
        for report in self:
            if f(report):
                result.append(report)
        return result

    def group(self, grouper: Callable[[ComputationReport], Hashable]) -> Dict[Any, "ComputationReportList"]:
        """
        Groups the reports according to the passed function. The passed function must return a
        hashable type, e.g. an integer, a string, or a tuple, that will be used as group identifier.

        Example: reports.group(lambda report: report.number_of_parties) groups the reports by the number of parties.
        """
        result = {}
        for report in self:
            result.setdefault(grouper(report), ComputationReportList()).append(report)
        return result

    def group_and_sort_groups(self,
                              grouper: Callable[[ComputationReport], Hashable],
                              key: Callable[[Tuple[Any, "ComputationReportList"]], int] = lambda a: a[0],
                              reverse: bool = False) -> List[Tuple[Any, "ComputationReportList"]]:
        groups = [(key, reports) for (key, reports) in self.group(grouper).items()]
        groups.sort(key=key, reverse=reverse)
        return groups

    def group_by_number_of_parties(self) -> Dict[int, "ComputationReportList"]:
        return self.group(lambda report: report.number_of_parties)

    def group_by_number_of_parties_sorted(self) -> List[Tuple[Any, "ComputationReportList"]]:
        return self.group_and_sort_groups(lambda report: report.number_of_parties)

    def group_by_delay(self) -> Dict[Optional[str], "ComputationReportList"]:
        return self.group(lambda report: report.delay)

    def group_by_outgoing_bandwidth(self) -> Dict[Optional[str], "ComputationReportList"]:
        return self.group(lambda report: report.outgoing_bandwidth)

    def group_by_network(self) -> Dict[Network, "ComputationReportList"]:
        return self.group(lambda report: report.network)

    def save_to_file(self, filename: str) -> None:
        with open(filename, 'w') as f:
            json.dump([report.to_serializable_dict() for report in self], f, indent=4)

    @staticmethod
    def from_folder(foldername: str) -> "ComputationReportList":
        result = ComputationReportList()
        for file in os.listdir(foldername):
            if file.startswith("computation-") and os.path.isfile(join_path_abs(foldername, file)):
                result.append(ComputationReport.from_file(join_path_abs(foldername, file)))
            elif os.path.isdir(join_path_abs(foldername, file)):
                result.extend(ComputationReportList.from_folder(join_path_abs(foldername, file)))
        return result

    @staticmethod
    def from_logs(timestamp: str) -> "ComputationReportList":
        """Given the timestamp that identifies a log, that may or may not be compressed, it will load the computation reports from that log.
        If the log is compressed, it will be extracted first."""

        # Check if the corresponding log is extracted.
        log_root_folder = join_path_abs(get_cdir(), "..", "logs")
        timestamp_folder = join_path_abs(log_root_folder, timestamp)
        if os.path.isdir(timestamp_folder):
            return ComputationReportList.from_folder(timestamp_folder)

        # Check if the zip file for that log exists.
        zip_filename = join_path_abs(log_root_folder, timestamp + '.tar.zst')
        if not os.path.isfile(zip_filename):
            raise NeonException(f"Can't find old log with timestamp \"{timestamp}\".")

        # Prepare the extraction destination.
        extraction_folder = join_path_abs(log_root_folder, 'extracted')
        os.mkdir(extraction_folder)

        # Extract, load computation reports, delete extracted data.
        subprocess.run(['tar', '-I', 'zstd', '-xvf', zip_filename], check=True, cwd=extraction_folder)

        result = ComputationReportList.from_folder(extraction_folder)
        shutil.rmtree(extraction_folder)
        return result
